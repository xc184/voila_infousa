# voila_infousa

My email for slack is 
xc184@duke.edu Thanks !

### meeting notes: March 6, 2023
- Add option to subset by a name
- ox is https://osmnx.readthedocs.io/en/stable/
- adapt get_city_polygon, https://github.com/siavash-saki/tesspy/tree/main/tesspy
- library: https://github.com/siavash-saki/tesspy/blob/main/tesspy/tessellation.py

backend process will accept a JSON file that specifies what data to fetch

frontend Voila app will write out JSON files specifying what the user asked for

when user asks for all of the USA, we can optimize by just returning the entire dataset (instead of telling the backend a list of all zip codes or a shape file that covers all of the USA)

Celine / Alyssa will specify the format of the JSON text file that the backend needs

## architecture

Users interact with the Voila web app to specify what part of the InfoUSA dataset they want. The
dataset can be filtered based on
- date (by year)
- zip code
- shape file
- place name

Users can enter or upload a list of zip codes for a given time period. 

Alternatively, users can upload a shape file and the zip codes which fall within that shape file will be used to filter the dataset. 

Another option is for users to provide a place name (such as New York City) and a clever library converts the place name into a shape file, which is then sent to the backend to filter the database.

The backend process accepts a JSON file describing the user's query, filters the InfoUSA data, and places the result in the user's directory.

The Voila web front end gets the user's identity from an OS-level enviromment variable - the assumption is that Voila is run in a contect that is authenticated and sets an ENV varibale with the user's netid.

 

## Getting started



### Prerequisites
Before using this document, you should have the following:

login to uses the Slurm scheduler. 

A Jupyter notebook environment, such as JupyterLab or Jupyter Notebook. 

The batch_merge.sh script and any other necessary files
### Steps
- Clone this repo & install packages from requirement.txt
- type thos command in your terminal`voila voila.ipynb `
- interactive with the pump up browser :  
- Attach the button click event handler to the "Run Batch Merge" button.
- Display the input widgets and "Run Batch Merge" button.
- Enter the user ID and select the desired options from the input widgets.
- Click the "Run Batch Merge" button to execute the batch_merge.sh script.
### Input Widgets
The input widgets are used to enter the user ID and select the desired options for the batch_merge.sh script.

User ID: Enter a user ID to use when executing the batch_merge.sh script.
State: Select a state from the dropdown list to use as the infousa_in argument for the batch_merge.sh script.
Time: Enter a time value to use as the output_in argument for the batch_merge.sh script.
"Run Batch Merge" Button
The "Run Batch Merge" button is used to execute the batch_merge.sh script with the selected options.

When the "Run Batch Merge" button is clicked, the on_run_button_click function is executed. This function retrieves the values of the input widgets, prints them to the console to double-check with the user, and then calls the run_batch_merge function, passing in the user ID and the selected state.

The run_batch_merge function constructs the command to execute the batch_merge.sh script, passing in the appropriate arguments, and then activates the script using the os.system function. Finally, a message is printed to the console indicating that the job has started.


### Conclusion
By following the steps in this document, you can use a browser to run the batch_merge.sh script on a system that uses the Slurm scheduler and save the merged file in the user's repo
