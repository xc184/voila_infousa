from flask import Flask, render_template, request, redirect, url_for
import os
import json
from werkzeug.utils import secure_filename
from zipfile import ZipFile

# 'convert_to_zipcodes' function
from uszipcode import SearchEngine
from zipfile import ZipFile
import shapefile

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


def convert_to_zipcodes(city, state):
    search = SearchEngine()
    zipcodes = []

    state_results = search.by_city_and_state(city, state, returns=sys.maxsize)
    if state_results:
        zipcodes.extend(state_results)
    zipcode_list = [zipcode.zipcode for zipcode in zipcodes]
    # convert to numeric
    zipcode_list = [int(i) for i in zipcode_list]
    return zipcode_list


def get_job_id():
    job_count = 0
    if os.path.exists("job.log"):
        with open("job.log", "r") as file:
            job_count = int(file.read().strip())
        job_count += 1
    with open("job.log", "w") as file:
        file.write(str(job_count))
    return job_count


def process_file(file, file_type, userid, years, filename):
    job_id = get_job_id()
    processing_folder = "processing_data"
    if not os.path.exists(processing_folder):
        os.makedirs(processing_folder)

    if file_type == "zipcodes":
        filepath = os.path.join(
            processing_folder, f"{job_id}_{secure_filename(file.filename)}")
        file.save(filepath)
        return {"zipcode_in": filepath, "spatial_in": "NA"}

    elif file_type == "cities":
        filepath = os.path.join(
            processing_folder, f"{job_id}_{secure_filename(file.filename)}")
        cities = file.read().decode("utf-8").strip().split("\n")
        zipcodes = []
        for city in cities:
            city_name, state_name = city.split(",")
            city_zipcodes = convert_to_zipcodes(
                city_name.strip(), state_name.strip())
            zipcodes.extend(city_zipcodes)
        with open(filepath, "w") as f:
            f.write("\n".join(str(z) for z in zipcodes))
        return {"zipcode_in": filepath, "spatial_in": "NA"}

    elif file_type == "shapefile":
        filepath = os.path.join(
            processing_folder, f"{job_id}_{secure_filename(file.filename)}")
        file.save(filepath)
        with ZipFile(filepath, "r") as zip_ref:
            shp_files = [f for f in zip_ref.namelist() if f.endswith(".shp")]
            if len(shp_files) == 0:
                return {"error": "The uploaded zip file does not contain a .shp file."}
            zip_ref.extractall(processing_folder)
        return {"zipcode_in": "NA", "spatial_in": os.path.join(processing_folder, shp_files[0])}

    else:
        return {"error": "Invalid file type."}


def save_user_data(userid, filename, years, query_data):
    data = {
        'userid': userid,
        'filename': filename,
        'years': years,
        'query_data': query_data,
    }

    if not os.path.exists('user_data'):
        os.makedirs('user_data')

    with open(f'user_data/{userid}_{filename}.json', 'w') as file:
        json.dump(data, file)


@app.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        userid = request.form['userid']
        years = request.form['years'].split(',')
        filename = request.form['filename']
        file_type = request.form['file_type']

        file = request.files['file']
        if file:
            result = process_file(file, file_type, userid, years, filename)

            if "error" not in result:
                query_data = result.copy()
                query_data["upload_filename"] = file.filename
                save_user_data(userid, filename, years, query_data)
                return f"File '{filename}' processed and user data saved successfully.", 200
        else:
            return "No file provided.", 400


if __name__ == '__main__':
    app.run(debug=True)
